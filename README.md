# SoundCloud Rover
## Projet Laravel

Le nom rover a été spécialement choisi pour ce projet. Le concept étant unique il lui fallait un nom unique en son genre !

#### Introduction :
- Création de routes/vues/action
- Création du modèle (Chanson). Relation 1-N
- Affichage de toutes les chansons
- Lancement des chansons dans le lecteur audio

#### Insertion de chanson dans la base de données :
- Insertion d'une nouvelle chanson dans la base de données
- Upload de fichier

#### La page utilisateur :
- Affichage de la page d'un utilisateur
- Suivi des utilisateurs
- Relation N-N
- Mise en place du suivi

#### Formulaire de recherche :
- Formulaire de recherche
- Création d'une route pour la recherche
- Requete SQL particulières

#### Introduction à Ajax :
- C'est quoi Ajax
- Un exemple d'ajax
- Première intégration d'ajax dans laravel

#### Ajax + Push state:
- Push state : prise en compte de l'historique quand on fait du ajax
- Utilisation de la librairie jquery-pjax

#### Des notifications :
- Prise en compte de notifications lorsque l'on modifie le statut
- Utilisation de toastr
- Utilisation des sessions laravel et des flashs data

#### Jquery-Pjax et formulaires :
- La librairie jquery-pjax et les formulaires

#### Validation des formulaires :
- La validation des formulaires