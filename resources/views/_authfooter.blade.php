<footer class="py-5">
    <div class="container">
        <div class="copyright text-center">
            &copy; 2019 <a href="{{ url('/') }}" class="font-weight-bold ml-1">RoverCloud</a>
        </div>
    </div>
</footer>